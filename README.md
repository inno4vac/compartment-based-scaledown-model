# Compartment based scaledown model

**Compartment based scaledown model (cbsm)** is a Python package for creating compartments of a bioreactor.

## Example

```Python
from cbsm.compartmentalisation import CM

filename = "2_A310_60_BM_160rpm_FlowStats_003.xlsx"
# Read CFD data
data = pd.read_excel(filename)
CFD_data = data.values
# Extract data
# start at 5 to avoid the header
X = CFD_data[1:, 9].tolist()
Y = CFD_data[1:, 1].tolist()
Vx = CFD_data[1:, 7].tolist()
Vy = CFD_data[1:, 6].tolist()

(grid_info, volume, img) = CM(X, Y, Vx, Vy)
```


## Install

Install the latest version of **cbsm**:

`pip install cbsm`

## Bugs

Please report any bugs using the [issues](https://gitlab.gbar.dtu.dk/inno4vac/compartment-based-scaledown-model/-/issues) or contribute and fix the bug by following the steps in the [Contribution Guide](https://gitlab.gbar.dtu.dk/inno4vac/compartment-based-scaledown-model/-/blob/main/CONTRIBUTING.md). We welcome all contributions!

## License

Released under the **MIT license** (see LICENSE.txt)
