from cbsm import splitting as splitting
import numpy as np


center_info = np.array([[1.0, 1.0, 1.0], [1.5, 1.5, 1.0], [3.0, 3.0, 1.0]])
delta_x = 1.0
delta_y = 1.0

split_result = splitting.split(center_info, delta_x, delta_y)
