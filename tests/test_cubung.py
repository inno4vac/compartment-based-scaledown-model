from cbsm.cubing2 import cubing2
import numpy as np

def test_4x4to2x2():
    zone_map = np.array([[0,0,0,0,1],[1,0,0,0,2],[2,0,0,0,2],[3,0,0,0,2],
                         [0,1,0,0,1],[1,1,0,0,1],[2,1,0,0,2],[3,1,0,0,3],
                         [0,2,0,0,3],[1,2,0,0,1],[2,2,0,0,4],[3,2,0,0,4],
                         [0,3,0,0,3],[1,3,0,0,3],[2,3,0,0,4],[3,3,0,0,4]])
    number_X = 2
    number_Y = 2
    delta_X = 3 / number_X
    delta_Y = 3 / number_Y
    cubing2_result = cubing2(zone_map, delta_X, delta_Y, number_X, number_Y)
    assert cubing2_result.shape[0] == 4
    assert cubing2_result[:, 4].tolist() == [1, 2, 3, 4]